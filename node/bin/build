#! /usr/bin/env node
/***********************************************************************
* Create production builds of the apps.
***********************************************************************/
process.env.NODE_ENV = 'production';

const webpack = require('webpack');
const { join } = require('path');
const { writeFileSync } = require('fs');
const {
  APPS,
  IS_INTERACTIVE,
  SERVER_DOCUMENT_ROOT,
} = require('./library/constants');
const {
  error,
  info,
  onWebpackComplete,
} = require('./library/methods');
const webpackConfigurationFactory = require('./library/webpack-configuration-factory');
const templateFactory = require('./library/template-factory');

const [,, TARGET_APP] = process.argv;

const ERROR_CODE = 1;

function abort(message) {
  // ZS-FIXME: refactor as required method
  error(message);
  process.exit(ERROR_CODE);
}

/**
 * Write the SPA entry HTML template to disc.
 *
 * @param {string} name
 *   The app name (cf. `manifest.json` in the app directory)
 */
function writeTemplate(name) {
  const fileName = join(SERVER_DOCUMENT_ROOT, name, 'index.html');
  // ZS-TODO:
  // Read assets from the file system after compilation
  // once `[hash]` is used and pass them through to the
  // template factory.
  const template = templateFactory({
    id: name,
  });

  writeFileSync(fileName, template);
}

/**
 * Build a queue of apps.
 *
 * @param {Array<Object>} appQueue
 * @return {Promise}
 */
function buildApps(appQueue) {
  const buildQueue = appQueue
    .map(({ name, source }) =>
      new Promise(function executor(resolve, reject) {
        const configuration = webpackConfigurationFactory(name, source);

        info(`Building ${name}...`);
        webpack(configuration, (...args) => {
          onWebpackComplete(...args);
          writeTemplate(name);
          resolve(name);
        });
      }));

  info('Build starting.');

  return Promise
    .all(buildQueue)
    .then(function done(queue) {
      info(`Apps built: ${queue}`);
    });
}

// ZS-FIXME: `IS_INTERACTIVE` isn't used yet
if (IS_INTERACTIVE) {
  // In development, offer an interactive prompt to choose the app to build.
  if (TARGET_APP) {
    abort('Arguments are not supported in this environment, use the interactive prompt');
  }

  const { prompt } = require('inquirer');
  const { mapAppsToChoices } = require('./library/methods');

  const ALL = '(all)';
  const choices = [...mapAppsToChoices(APPS), ALL];
  const questions = [
    {
      choices,
      default: null,
      message: 'Select an App to build',
      name: 'app',
      type: 'list',
    },
  ];

  const resolve = function resolve({ app }) {
    const queue = (app === ALL) ? APPS : [app];

    buildApps(queue);
  };

  prompt(questions)
    .then(resolve);
} else {
  // Integration build: a single target can
  // be provided as an optional argument.
  if (TARGET_APP) {
    const app = APPS
      .find(({ name }) =>
        (name === TARGET_APP));

    if (!app) {
      abort(`Unknown app ${TARGET_APP}`);
    }

    buildApps([app]);
  } else {
    buildApps(APPS);
  }
}
