/****************************
 * Webpack app configuration.
 */
const { join } = require('path');
const {
  IS_DEVELOPMENT,
  IS_EDGE,
  SERVER_DOCUMENT_ROOT,
} = require('./constants');
const baseConfiguration = require('./webpack/base');

const { assign } = Object;

/**
 * Webpack configuration factory.
 * @see https://webpack.js.org/configuration/
 * @private
 *
 * @param {string} app
 *   App name
 * @param {string} source
 *   App source directory
 * @returns {Object}
 *   Webpack configuration object
 */
function webpackConfigurationFactory(app, source) {
  const { entry } = require(join(source, 'manifest.json'));
  const configuration = assign(baseConfiguration, {
    entry: {
      [app]: join(source, entry),
    },
    output: {
      filename: '[name].js',
      path: join(SERVER_DOCUMENT_ROOT, app),
    },
  });

  if (IS_DEVELOPMENT) {
    const serve = require('./webpack/serve');

    serve(configuration);
  } else {
    const build = require('./webpack/build');

    build(app, configuration);
  }

  if (IS_EDGE) {
    const edge = require('./webpack/edge');

    edge(configuration);
  }

  return configuration;
}

module.exports = webpackConfigurationFactory;
