const whitelist = require('./whitelist');
const { keys } = Object;

const isValidPackage = (context, name) =>
  keys(whitelist[context])
    .some(key => new RegExp(`^${key}(@|$)`).test(name));

module.exports = isValidPackage;
