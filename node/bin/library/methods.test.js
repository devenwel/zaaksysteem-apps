const { join } = require('path');
const {
  getParentDirectory,
  mapManifestToApp,
} = require('./methods');
const { NODE_ROOT } = require('./constants');

const STUB = join(NODE_ROOT, 'bin', 'test', 'stub');

describe('The `getParentDirectory` function', () => {
  test('returns the second last path segment', () => {
    expect(getParentDirectory('foo/bar/baz/quux')).toBe('baz');
  });
});

describe('The `mapManifestToApp` function', () => {
  test('throws if the manifest does not exist', () => {
    expect(() => mapManifestToApp(join(STUB, 'dev', 'null'))).toThrow();
  });

  test('returns the manifest name property if it is set', () => {
    expect(mapManifestToApp(join(STUB, 'manifest-name.json'))).toBe('foobar');
  });

  test('returns the manifest parent directory if the name property is not set', () => {
    expect(mapManifestToApp(join(STUB, 'manifest-no-name.json'))).toBe('stub');
  });
});
