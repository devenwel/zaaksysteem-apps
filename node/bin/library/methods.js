/***************************************************************
 * Methods must be testable and runtime environment independent.
 */

const { gray, red, yellow } = require('chalk');
const { dirname } = require('path');

// file system

const getDirectory = filePath =>
  dirname(filePath);

const mapManifestToApp = manifestFile => ({
  name: require(manifestFile).name,
  source: getDirectory(manifestFile),
});

const getAppQueue = manifestQueue =>
  manifestQueue
    .map(mapManifestToApp);

// feedback

function error(message) {
  console.error(`${red('ZS ERROR')}: ${message}`);
}

function info(message) {
  console.info(`${gray('ZS INFO')}: ${message}`);
}

function warn(message) {
  console.warn(`${yellow('ZS WARNING')}: ${message}`);
}

// misc.

/**
 * Map apps to `Inquirer` choices.
 *
 * @type {Array<Object>}
 */
const mapAppsToChoices = apps =>
  apps
    .map(app => ({
      name: app.name,
      value: app,
    }));

// webpack

/**
 * Webpack node API callback.
 * @see https://webpack.js.org/api/node/#error-handling
 *
 * @param error
 * @param stats
 */
function onWebpackComplete(webpackError, stats) {
  // ZS-FIXME: refactor complexity
  /* eslint complexity: ["error", 6] */
  if (webpackError) {
    error(webpackError.stack || webpackError);

    if (webpackError.details) {
      error(webpackError.details);
    }

    return;
  }

  const jsonStats = stats.toJson();

  if (stats.hasErrors()) {
    error(jsonStats.errors);
  }

  if (stats.hasWarnings()) {
    warn(jsonStats.warnings);
  }

  info('WEBPACK DONE');
}

module.exports = {
  getDirectory,
  mapAppsToChoices,
  mapManifestToApp,
  getAppQueue,
  error,
  info,
  warn,
  onWebpackComplete,
};
