const exec = require('./exec');
const DELETE = 0;
const TAIL_OFFSET = 1;

/**
 * @param {Array} list
 * @param {string} context
 * @return {Array}
 */
function getContextArguments(list, context) {
  if (context === 'build') {
    return list;
  }

  const offset = (list.length - TAIL_OFFSET);
  const prefix = ['--prefix', `./${context}`];

  list.splice(offset, DELETE, ...prefix);

  return list;
}

/**
 * @param {string} context
 * @param {Array} argumentList
 * @return {Promise}
 */
const npmFactory = (context, argumentList) =>
  exec('npm', getContextArguments(argumentList, context));

/**
 * @param {string} context
 * @param {string} packageName
 * @return {Promise}
 */
const install = (context, packageName) =>
  npmFactory(context, [
    'install',
    '-no-optional',
    '-s',
    packageName,
  ]);

/**
 * @param {string} context
 * @param {string} packageName
 * @return {Promise}
 */
const uninstall = (context, packageName) =>
  npmFactory(context, [
    'uninstall',
    '-s',
    packageName,
  ]);

module.exports = {
  install,
  uninstall,
};
