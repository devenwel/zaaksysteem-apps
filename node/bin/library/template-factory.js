const { IS_DEVELOPMENT } = require('./constants');

const getBaseUrl = id => {
  if (IS_DEVELOPMENT) {
    const { PUBLIC_PATH } = require('./development');

    return `${PUBLIC_PATH}${id}.js`;
  }

  return `/${id}/${id}.js`;
};

const getStyleSheet = id => {
  if (IS_DEVELOPMENT) {
    return '<!-- PLACEHOLDER -->';
  }

  return `<link href="/${id}/${id}.css" rel="stylesheet">`;
};

/**
 * SPA entry file factory.
 * ZS-TODO:
 * - use hashed files in production
 * - use smart async script loader
 *
 * @param {Object} options
 * @param {string} [options.content]
 * @param {string} options.id
 * @param {string} [options.lang=nl]
 * @param {string} [options.title=Zaaksysteem]
 * @returns {string}
 */
const TemplateFactory = ({
  content = '',
  id,
  lang = 'nl',
  title = 'Zaaksysteem',
}) => `
<!DOCTYPE html>
<html lang="${lang}">
  <head>
    <meta charset="UTF-8">
    <meta
      name="viewport"
      content="width=device-width,initial-scale=1.0"
    >
    <meta
      http-equiv="X-UA-Compatible"
      content="ie=edge"
    >
    ${getStyleSheet(id)}
    <title>${title}</title>
  </head>
  <body>

    <div id="zs-app">${content}</div>
    
    <script src="/vendor/react.js"></script>
    <script src="/vendor/ui.js"></script>
    <script src="${getBaseUrl(id)}"></script>

  </body>
</html>
`;

module.exports = TemplateFactory;
