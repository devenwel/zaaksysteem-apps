#!/bin/bash

if [ "$@" ]; then
    docker build --target=$1 -t registry.gitlab.com/zaaksysteem/zaaksysteem-apps:dev .
else
    docker build --target=development -t registry.gitlab.com/zaaksysteem/zaaksysteem-apps:dev .
fi