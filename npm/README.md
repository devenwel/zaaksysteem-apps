# `./npm`

> `npm` registry dependencies. The package and lock files are under
  version control. They are copied to `../node` in the container and 
  must be copied back after changes are made in the container with 
  `npm run export`.

## What problem does this solve?

`package.json` and `node_modules` are siblings in the file system by 
design (workarounds for that introduce different problems). You don't
want `node_modules` mounted on the host and mounting `package*.json`
files individually breaks synchronization on mutation.

## `./build`

Build and QA dependencies, used to build the client-side app and vendor bundle(s).

## `./vendor`

Client-side dependencies that are installed and rarely change.

## Container structure

Since `node_modules` are only installed in the container, the structure
is changed so that the vendor `run` scripts can resolve the shared 
build modules upwards.

- `/opt/zaksysteem-apps/node`
    - `package.json`
    - `package-lock.json`
    - `/vendor`
        - `package.json`
        - `package-lock.json`
