const PWD = process.cwd();

module.exports = {
  bail: true,
  coverageDirectory: '<rootDir>/public/coverage',
  moduleNameMapper: {
    // Generic return values for otherwise ignored
    // imports that would throw a syntax error:
    '\\.css$': '<rootDir>/src/test/double/dummy/style.js',
  },
  modulePaths: [
    '<rootDir>/vendor/node_modules',
  ],
  rootDir: PWD,
  setupTestFrameworkScriptFile: '<rootDir>/src/test/jest.setup.js',
  testMatch: [
    '<rootDir>/src/**/*.test.js',
  ],
  testURL: 'https://dev.zaaksysteem.nl/',
};
