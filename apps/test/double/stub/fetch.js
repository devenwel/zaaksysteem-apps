let cannedAnswer;

/**
 * @return {Object}
 */
function getCannedAnswer() {
  const data = cannedAnswer;

  setCannedAnswer();

  return data;
}

/**
 * @param {Object} [data]
 */
function setCannedAnswer(data = {}) {
  cannedAnswer = data;
}

/**
 * @return {Promise<any>}
 */
const json = () =>
  Promise
    .resolve(getCannedAnswer());

/**
 * @return {Promise<any>}
 */
const fetch = () =>
  Promise
    .resolve({
      json,
    });

fetch.stub = setCannedAnswer;
setCannedAnswer();

module.exports = fetch;
