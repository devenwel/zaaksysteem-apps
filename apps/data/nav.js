/**
 * @type {Array}
 */
export const drawerActions = [
  {
    selected: true,
    icon: 'home',
    label: 'Honkblad',
  },
  {
    selected: false,
    icon: 'search',
    label: 'Zoeken',
  },
];

/**
 * @type {Array}
 */
export const tabsActions = [
  {
    url: '/next/configuratie',
    label: 'Standaard opties',
  },
  {
    url: '/next/configuratie/zaaksysteem',
    label: 'Zaaksysteem opties',
  },
  {
    url: '/next/configuratie/geavanceerd',
    label: 'Geavanceerde opties',
  },
];
