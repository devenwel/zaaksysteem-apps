import { asArray } from './array';

/**
 * @param {Array} queue
 *   The callbacks to be run.
 * @param {*} parameters
 *   The parameters to run each callback with.
 */
export function runQueue(queue, parameters) {
  for (const callback of queue) {
    callback(...asArray(parameters));
  }
}
