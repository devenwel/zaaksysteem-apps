/**
 * @param {*} value
 * @return {boolean}
 */
export const isDefined = value =>
  typeof value !== 'undefined';

/**
 * @param {*} value
 * @return {boolean}
 */
export const hasValue = value =>
  value !== '' &&
  value !== null &&
  value !== [];
