import i18n from 'i18next';
import locale from '../../data/locale/locale';
import { reactI18nextModule } from 'react-i18next';

i18n
  .use(reactI18nextModule)
  .init({
    lng: 'nl-NL',
    fallbackLng: 'en-US',
    resources: locale,
    debug: false,
    react: {
      wait: true,
    },
  });

export default i18n;
