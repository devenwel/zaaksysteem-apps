/**
 * 
 * ZS-TODO: duplicated and modified from @mintlab/UI -- move this to a seperate NPM module and
 * use that here and in @mintlab/UI
 * 
 * Traverse the map, and return the output of the 
 * first value function where the key function is truthy
 * 
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
 * 
 * @param {Object} parameters
 * @param {Map} parameters.map
 * @param {Array} [parameters.keyArgs=[]]
 * @param {Array} [parameters.functionArgs=[]]
 * @param {*} [parameters.fallback=undefined]
 * @return {*|undefined}
 */
/* eslint complexity: [2, 4] */
export function traverseMap({
  map,
  keyArgs=[],
  functionArgs=[],
  fallback=undefined}) {

  for (let [mapKey, mapFunction] of map) {
    if(mapKey(...keyArgs)) {
      return mapFunction(...functionArgs);
    }
  }

  if (typeof fallback === 'function') {
    return fallback(...functionArgs);
  }

  return fallback;
}
