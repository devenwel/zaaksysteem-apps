import emailRegex from 'email-regex';
import { hasValue } from '../../../library/value';

/**
 * @param {string} userInput
 * @returns {boolean}
 */
export const email = userInput => {
  if (!hasValue(userInput)) {
    return true;
  }

  return emailRegex({ exact: true }).test(userInput);
};
