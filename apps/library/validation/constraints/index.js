export { email } from './email';
export { number } from './number';
export { uri } from './uri';
export { required } from './required';
