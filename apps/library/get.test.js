import get from './get';

/**
 * @test {get}
 */
describe ('The `get` module exports a function that', () => {
  test('returns the value of an object property', () => {
    const object = {
      foo: 'bar',
    };
    const actual = get(object, 'foo');
    const expected = 'bar';

    expect(actual).toBe(expected);
  });

  test('returns the value of a deep object property', () => {
    const object = {
      foo: {
        bar: 'quux',
      },
    };
    const actual = get(object, 'foo.bar');
    const expected = 'quux';

    expect(actual).toBe(expected);
  });

  test('supports bracket property accessors', () => {
    const object = {
      foo: {
        bar: 'quux',
      },
    };
    const actual = get(object, 'foo[bar]');
    const expected = 'quux';

    expect(actual).toBe(expected);
  });

  test('returns `undefined` for an object property that does not exist', () => {
    const object = {};
    const actual = get(object, 'nil');
    let expected;

    expect(actual).toBe(expected);
  });

  test('returns `undefined` for a deep object property that does not exist', () => {
    const object = {};
    const actual = get(object, 'foo.bar');
    let expected;

    expect(actual).toBe(expected);
  });
});
