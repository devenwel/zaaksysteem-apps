const { isArray } = Array;

/**
 * @param {*} value
 * @return {Array}
 */
export function asArray(value) {
  if (isArray(value)) {
    return value;
  }

  return [value];
}

/**
 * Mutate a given array by removing a given element.
 *
 * @param {Array} array
 *   The array to mutate.
 * @param {*} value
 *   The element to remove.
 */
export function removeFromArray(array, value) {
  const index = array.indexOf(value);
  const length = 1;

  array.splice(index, length);
}

/**
 * @param {Array} array
 * @return {boolean}
 */
export function isPopulatedArray(array) {
  return Boolean(isArray(array) && array.length);
}
