import { traverseMap } from './map';

const { stringify } = JSON;

describe ('The `map` module exports', () => {
  /**
   * @test {traverseMap}
   */
  describe('a `traverseMap` function that', () => {
    const keyArgs = ['a', 'b'];
    const functionArgs = ['c', 'd'];
    const FALLBACK_VALUE = 'foo';
    const map = new Map([
      [() => false, () => false],
      [(first, second) => second === 'b', (third, fourth) => fourth],
    ]);

    test('returns the output of first value function where the key function is truthy', () => {
      const actual = traverseMap({
        map,
        keyArgs,
        functionArgs,
      });
      const [, expected] = functionArgs;

      expect(stringify(actual) === stringify(expected)).toBe(true);
    });

    test('returns the fallback value if there are no matches', () => {
      const actual = traverseMap({
        map,
        fallback: FALLBACK_VALUE,
      });

      expect(actual).toBe(FALLBACK_VALUE);
    });

    test('supports fallback as a function, with the functionArgs supplied', () => {
      const actual = traverseMap({
        map,
        functionArgs,
        fallback: (first, second) => second,
      });

      const [, expected] = functionArgs;
      expect(actual).toBe(expected);
    });

  });
});
