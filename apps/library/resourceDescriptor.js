import resource from '../App/Resource';

const isObject = value =>
  value !== null
  && typeof value == 'object';

const isData = value =>
  isObject(value);

const isId = value =>
  typeof value == 'string';

function isOrderedPairTuple(parameters) {
  const [id, data] = parameters;

  return isId(id) && isData(data);
}

const isIdOrOrderedPairTuple = value =>
  isId(value)
  || isOrderedPairTuple(value);

const isIdSet = parameters =>
  parameters
    .every(item =>
      isId(item));

const isIdOrOrderedPairTupleSet = parameters =>
  Array.isArray(parameters)
  && parameters
    .every(isIdOrOrderedPairTuple);

/* eslint complexity: [2, 10] */
export function normalize(parameters) {
  if (isId(parameters)) {
    return [[parameters], [resource(parameters)]];
  }

  if (isOrderedPairTuple(parameters)) {
    const [id, data] = parameters;

    return [[id], [resource(id, data)]];
  }

  if (isIdSet(parameters)) {
    return [
      parameters,
      parameters
        .map(id => resource(id)),
    ];
  }

  if (isIdOrOrderedPairTupleSet(parameters)) {
    return [
      parameters,
      parameters
        .map(id => resource(id)),
    ];
  }

  throw new TypeError('bad arguments');
}

export function getKeys(descriptor) {
  if (isId(descriptor)) {
    return [descriptor];
  }

  if (isOrderedPairTuple(descriptor)) {
    const [id] = descriptor;

    return [id];
  }

  if (isIdSet(descriptor)) {
    return descriptor;
  }

  if (isIdOrOrderedPairTupleSet(descriptor)) {
    return descriptor
      .map(item => {
        if (isOrderedPairTuple(item)) {
          const [id] = item;

          return id;
        }

        return item;
      });
  }
}
