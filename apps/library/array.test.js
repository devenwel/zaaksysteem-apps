import {
  asArray,
  removeFromArray,
  isPopulatedArray,
} from './array';

const MAGIC_ANSWER = 42;
const MAGIC_ARRAY = ['a', 'b', 'c'];

describe('The `array` module', () => {
  /**
   * @test {asArray}
   */
  describe('exports an `asArray` function that', () => {
    test('wraps a value that is not an array in an array', () => {
      const actual = asArray(MAGIC_ANSWER);
      const expected = [MAGIC_ANSWER];

      expect(actual).toEqual(expected);
    });

    test('passes through a value that is an array itself', () => {
      const expected = [MAGIC_ANSWER];
      const actual = asArray(expected);

      expect(actual).toBe(expected);
    });
  });

  /**
   * @test {removeFromArray}
   */
  describe('exports a `removeFromArray` function that', () => {
    test('mutates an array by removing one of its elements', () => {
      const actual = ['foo', 'bar', 'quux'];
      const expected = ['foo', 'quux'];

      removeFromArray(actual, 'bar');
      expect(actual).toEqual(expected);
    });
  });

  describe('exports a `isPopulatedArray` function that', () => {
    test('evaluates whether the value is an array and has at least one entry', () => {
      expect(isPopulatedArray([])).toEqual(false);
      expect(isPopulatedArray(MAGIC_ARRAY)).toEqual(true);
      expect(isPopulatedArray(MAGIC_ANSWER)).toEqual(false);
    });

  });
});
