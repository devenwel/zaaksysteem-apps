const {
  assign,
  create,
  defineProperties,
  keys,
} = Object;

/**
 * Create a clean Object without a prototype,
 * with properties that are neither mutable
 * nor enumerable.
 *
 * @param {Object} interfaceObject
 * @return {Object}
 */
export default function dictionary(interfaceObject) {
  const reduceInterface = (descriptors, propertyName) => assign(descriptors, {
    [propertyName]: {
      enumerable: true,
      value: interfaceObject[propertyName],
    },
  });

  return defineProperties(
    create(null),
    keys(interfaceObject).reduce(reduceInterface, create(null))
  );
}
