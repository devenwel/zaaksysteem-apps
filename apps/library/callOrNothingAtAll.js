/**
 * Call a value with optional arguments if it is a function
 *
 * @param {*} callback A value that is called if it is a function
 * @param {Array} [args=[]] The optional arguments for the callback
 * @returns {*}
 */
export default function callOrNothingAtAll(callback, args = []) {
  if (typeof callback === 'function') {
    return callback(...args);
  }
}
