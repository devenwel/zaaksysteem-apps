import { request } from './JSON';
import { map, transformers } from './API';
import * as fixtures from './Fixtures';

const { assign } = Object;

/**
 * @param {Object} options
 * @param {Object} options.json
 *   Original API response data
 * @param {string} options.url
 *   API URL to match tranformers against
 * @return {Object}
 */
export const mangleResponse = ({ json, url }) => {
  for (const { match, transform } of transformers) {
    if (match.some(literal => (new RegExp(literal)).test(url))) {
      return transform(json, url);
    }
  }

  return json;
};

const getKeyValuePair = (key, value) =>
  [key, value]
    .map(literal => encodeURIComponent(literal))
    .join('=');

const toQueryString = (...query) =>
  query
    .filter(([, value]) => value !== undefined)
    .map(([
      key,
      value,
    ]) => getKeyValuePair(key, value))
    .join('&');

/* eslint complexity: [2, 10] */
const getRequestUrl = (url, parameters = {}) => {
  if (!parameters) {
    return url;
  }

  let entries = Object.entries(parameters).map(([name, value]) => {
    const replace = {
      'rows': 'rows_per_page',
    };
    return [(replace[name]) ? replace[name] : name, value];
  });

  const query = toQueryString(...entries);

  if (query.length) {
    return `${url}?${query}`;
  }
};

const getStoreData = ({
  json,
  method,
  url,
  meta,
}) => assign({}, {
  data: url ?
    mangleResponse({
      json,
      url,
    }) :
    json,
  method,
  status: json.status_code,
  timestamp: Number(Date.now()),
  url,
  meta,
});

function getRestParameters(method, url, data) {
  if (method === 'GET') {
    return [getRequestUrl(url, data)];
  }
  return [url, data];
}

/**
 * @param {Object} config
 * @param {Object} [data]
 * @return {Promise}
 */
const apiRequest = ({
  method,
  url,
  meta,
}, data) =>
  request(method, ...getRestParameters(method, url, data))
    .then(json => getStoreData({
      json,
      method,
      url,
      meta,
    }));

/**
 * @param {Object} fixtureData
 * @return {Promise}
 */
/*eslint-disable */
const resolveFixture = fixtureData => Promise
  .resolve(fixtureData)
  .then(resolvedData =>
    resolvedData.json ? getStoreData({...resolvedData}) : getStoreData({ json: resolvedData })
  );
/*eslint-enable*/

/**
 * Resolve an internal resource ID with fixture data or API network requests,
 * or perform a custom network request.
 *
 * @example
 * resource('foo');
 * resource({
 *   id: 'github_users',
 *   method: 'GET',
 *   url: 'https://api.github.com/search/users', *
 * });
 *
 * @param {string|Object} idOrOptions
 *  Resource ID or an options object with the signature
 *    - `{string}` id
 *    - `{string}` method
 *    - `{string}` url
 * @param {Object} [data]
 *  Resource query parameters.
 * @return {Promise}
 */
export default function resource(idOrOptions, data) {
  if (typeof idOrOptions === 'object') {
    return apiRequest(idOrOptions, data);
  }

  const fixtureMap = fixtures[idOrOptions];

  if (fixtureMap) {
    return resolveFixture(fixtureMap);
  }

  const resourceMap = map[idOrOptions];

  if (resourceMap) {
    return apiRequest(resourceMap, data);
  }

  const reason = new Error(`Resource: Fatal: Unknown resource ID or map "${idOrOptions}"`);

  return Promise.reject(reason);
}
