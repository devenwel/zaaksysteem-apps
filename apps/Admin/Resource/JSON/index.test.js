import json from '.';
import { methods } from './fetch';

const HTTP_METHODS_LENGTH = 2;

/**
 * @test {json}
 */
describe('The Resource API module', () => {
  describe('exports an object that', () => {
    test('implements all HTTP methods of the `./json` module', () => {
      expect.assertions(HTTP_METHODS_LENGTH);

      for (const name of methods) {
        expect(typeof json[name.toLowerCase()]).toBe('function');
      }
    });

    test('throws when you try to reassign a method', () => {
      expect(() => {
        json.get = 42;
      }).toThrow();
    });
  });
});

