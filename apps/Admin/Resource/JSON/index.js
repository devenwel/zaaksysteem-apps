import dictionary from '../../../library/dictionary';
import { request, methods } from './fetch';

const { assign } = Object;

/**
 * Syntactic sugar for the `./json` module.
 */
const jsonApiInterface = methods
  .reduce((api, method) =>
    assign(api, {
      [method.toLowerCase()]: (url, body) => request(method, url, body),
    }), {});

/**
 * @type {Object}
 */
const json = dictionary(jsonApiInterface);

export { request };
export default json;
