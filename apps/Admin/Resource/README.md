# Resource

> The *Zaaksysteem client application* resolves resources
  with a side-effect that is triggered in the Store tier.

The `View` tier only needs to know about the resource ID,
the effect in the `Store` tier uses the `Resource` tier's
`./resolve` function: 

    import resource from '../../Resource';
    
    // ...
    resolve('SOME ID');
      .then(/**/)    
    // ...

## See also

- `./Fixtures`
    - *Resource ID* => *data* dictionary 
      (takes precedence over the API dictionary)
- `./Api`
    - `map.js`
        - *Resource ID* => *Request configuration* dictionary
