# API

> Interface the *Zaaksysteem client application* 
  with the *Zaaksysteem API*.

## Responsibilities

- **map** resource IDs to HTTP request arguments
- **transform** API response body data if necessary

## Zaaksysteem API Documentation

The Zaaksysteem API v1 documentation is available at `/man/Zaaksysteem::Manual::API`

Example: 
[quarterly](https://quarterly.zaaksysteem.nl/man/Zaaksysteem::Manual::API)
