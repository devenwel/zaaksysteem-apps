import {
  isEmpty,
  isArrayOfStrings,
  isArrayOfDbObjects,
  isDbObject,
  isSelect,
  isArrayOfSelects,
  selectToDb,
  arrayToDb,
  objectToSelect,
  arrayToSelect,
} from './transformers';

describe('The transformers library module', () => {
  /**
   * @test {isEmpty}
   */
  test('exports an `isEmpty` function', () => {
    expect(typeof isEmpty).toBe('function');
  });

  /**
   * @test {isArrayOfStrings}
   */
  test('exports an `isArrayOfStrings` function', () => {
    expect(typeof isArrayOfStrings).toBe('function');
  });

  /**
   * @test {isArrayOfDbObjects}
   */
  test('exports an `isArrayOfDbObjects` function', () => {
    expect(typeof isArrayOfDbObjects).toBe('function');
  });

  /**
   * @test {isDbObject}
   */
  test('exports an `isDbObject` function', () => {
    expect(typeof isDbObject).toBe('function');
  });

  /**
   * @test {isSelect}
   */
  test('exports an `isSelect` function', () => {
    expect(typeof isSelect).toBe('function');
  });

  /**
   * @test {isArrayOfSelects}
   */
  test('exports an `isArrayOfSelects` function', () => {
    expect(typeof isArrayOfSelects).toBe('function');
  });

  /**
   * @test {selectToDb}
   */
  test('exports an `selectToDb` function', () => {
    expect(typeof selectToDb).toBe('function');
  });

  /**
   * @test {arrayToDb}
   */
  test('exports an `arrayToDb` function', () => {
    expect(typeof arrayToDb).toBe('function');
  });

  /**
   * @test {objectToSelect}
   */
  test('exports an `objectToSelect` function', () => {
    expect(typeof objectToSelect).toBe('function');
  });

  /**
   * @test {arrayToSelect}
   */
  test('exports an `arrayToSelect` function', () => {
    expect(typeof arrayToSelect).toBe('function');
  });
});
