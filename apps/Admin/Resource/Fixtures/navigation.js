/**
 * Array of navigation link configurations with the signature
 * - `{string}` icon
 * - `{string}` label
 * - `{string}` path
 *
 * @type {Array<Object>}
 */
export const navigation = [
  {
    icon: 'chrome_reader_mode',
    label: 'Catalogus',
    path: '/admin/catalogus',
  },
  {
    icon: 'people',
    label: 'Gebruikers',
    path: '/admin/gebruikers',
  },
  {
    icon: 'import_contacts',
    label: 'Logboek',
    path: '/admin/logboek',
  },
  {
    icon: 'poll',
    label: 'Transacties',
    path: '/admin/transacties',
  },
  {
    icon: 'all_inclusive',
    label: 'Koppelingen',
    path: '/admin/koppelingen',
  },
  {
    icon: 'view_comfy',
    label: 'Gegevens',
    path: '/admin/gegevens',
  },
  {
    icon: 'settings',
    label: 'Configuratie',
    path: '/admin/configuratie',
  },
];
