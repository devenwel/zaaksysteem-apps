import { createElement } from 'react';
import { Layout } from '@mintlab/ui';
import View from './View';
import { logout } from '../../library/auth';
import { getSegment, navigate } from '../../../../library/url';
import { extract } from '../../../../library/object';

const SUPPORT_URL = 'https://help.zaaksysteem.nl/';

/**
 * @param {Array} data
 * @param {string} url
 * @return {number}
 */
const getNavigationIndex = (data, url) => {
  const segment = getSegment(url);
  const index = data
    .map(item => getSegment(item.path))
    .indexOf(segment);

  return index;
};

/**
 * @param {Array} input
 * @param {Function} route
 * @return {Array}
 */
const mapToDrawer = (input, route) =>
  input
    .map(({ icon, label, path }) => ({
      action() {
        route({
          path,
        });
      },
      icon,
      label,
    }));

/**
 * Wrap the {@link View} in the `@mintlab/ui` Layout.
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export default function AdminLayout(props) {
  const { t } = props;
  const [
    {
      data: {
        account: {
          instance: {
            company: customer,
          },
        }
      }
    },
    { data },
    isDrawerOpen,
    toggleDrawer,
    iframeProps,
  ] = extract('session', 'navigation', 'isDrawerOpen', 'toggleDrawer', props);
  const {
    requestUrl,
    route,
  } = iframeProps;

  const CurrentView = createElement(View, iframeProps);

  return createElement(Layout, {
    active: getNavigationIndex(data, requestUrl),
    customer,
    drawer: {
      primary: mapToDrawer(data, route),
      secondary: [
        {
          action: () => console.info('SWITCH'),
          icon: 'sync',
          label: 'Switch naar behandelen',
        },
        {
          action: () => navigate(SUPPORT_URL),
          icon: 'alarm',
          label: 'Support',
        },
      ],
      about: {
        action: () => console.info('ABOUT'),
        label: 'Versie 3.2.1',
      },
    },
    // ZS-TODO: arbitrary value, and check if left is actually needed
    gutter: 20,
    identity: `${t('common:title')} ${t('common:admin')}`,
    isDrawerOpen,
    menuLabel: t('aria:mainMenu'),
    toggleDrawer,
    userActions: [
      {
        action: logout,
        icon: 'power_settings_new',
        label: 'Log out',
      },
    ],
    userLabel: t('aria:userMenu'),
  }, CurrentView);
}
