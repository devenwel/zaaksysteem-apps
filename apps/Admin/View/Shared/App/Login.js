import { createElement } from 'react';
import LayoutContainer from './LayoutContainer';
import { isLoggedIn, login } from '../../library/auth';
import { getUrl } from '../../../../library/url';

/**
 * @param {Object} props
 * @param {Object} props.resource
 * @return {ReactElement|null}
 */
export default function Login({ resource }) {
  if (!isLoggedIn(resource.session)) {
    login(getUrl());

    return null;
  }

  return createElement(LayoutContainer, resource);
}
