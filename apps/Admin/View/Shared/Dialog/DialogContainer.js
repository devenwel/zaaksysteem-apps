import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import Dialog from './Dialog';
import Actions from '../../../Store/Action';

const mapStateToProps = ({ ui, route, form, resource }) => ({ ui, route, form, resource });

const mapDispatchToProps = dispatch => ({
  set: bindActionCreators(Actions.form.set, dispatch),
  hide: bindActionCreators(Actions.ui.dialog.hide, dispatch),
  invoke: bindActionCreators(Actions.route.invoke, dispatch),
});

const DialogContainer = translate()(connect(
  mapStateToProps,
  mapDispatchToProps
)(Dialog));

export default DialogContainer;
