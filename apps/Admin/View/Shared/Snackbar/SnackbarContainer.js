
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { bindActionCreators } from 'redux';
import SnackbarWrapper from './Snackbar';
import Actions from '../../../Store/Action';

const mapStateToProps = ({ ui: { snackbar }}) => ({ snackbar });
const mapDispatchToProps = dispatch => ({
  onQueueEmpty: bindActionCreators(Actions.ui.snackbar.clear, dispatch),
});

const SnackbarContainer = translate()(connect(
  mapStateToProps,
  mapDispatchToProps
)(SnackbarWrapper));

export default SnackbarContainer;
