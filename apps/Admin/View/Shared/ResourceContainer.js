import { connect } from 'react-redux';
import { Resource } from '@mintlab/ui';
import { asArray } from '../../../library/array';
import action from '../../Store/Action';

const { assign } = Object;

const {
  resource: {
    request,
  },
} = action;

const mapStateToProps = ({ resource }, { id }) => ({
  payload: asArray(id)
    .reduce((accumulator, key) => assign(accumulator, {
      [key]: (resource[key] || null),
    }), {}),
});

const mapDispatchToProps = dispatch => ({
  resolve: descriptor => dispatch(request(descriptor)),
});

const ResourceContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Resource);

export default ResourceContainer;
