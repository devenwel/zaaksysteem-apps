import { navigate } from '../../../library/url';

/*
 * Authentication and authorization utilities.
 */

const AUTH_PATH = '/auth';
export const LOGIN_PATH = [AUTH_PATH, 'login'].join('/');
export const LOGOUT_PATH = [AUTH_PATH, 'logout'].join('/');

/**
 * @param {Object} session
 * @return {boolean}
 */
export const isLoggedIn = session =>
  Boolean(session.data.logged_in_user);

/**
 * Redirect the current user to the login page.
 *
 * @param {string} referer
 */
export function login(referer) {
  navigate(`${LOGIN_PATH}?referer=${referer}`);
}

/**
 * Redirect the current user to the logout page.
 */
export function logout() {
  navigate(LOGOUT_PATH);
}
