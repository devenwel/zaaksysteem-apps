import { DEFAULT_SEGMENT, getDefaultRoute, normalizeRequestUrl } from './initialState';
import { multiLine as n } from '../../../../library/prettyPrint';

describe('The `initialState` module', () => {
  describe('uses a `getDefaultRoute` function that', () => {
    test(n(
      'appends the default route segment',
      'if the path component argument does end with a hierarchy character'
    ), () => {
      const actual = getDefaultRoute('/foo/');
      const expected = `/foo/${DEFAULT_SEGMENT}`;

      expect(actual).toBe(expected);
    });

    test(n(
      'appends a hierarchy character and the default route segment',
      'if the path component argument does not end with a hierarchy character'
    ), () => {
      const actual = getDefaultRoute('/foo');
      const expected = `/foo/${DEFAULT_SEGMENT}`;

      expect(actual).toBe(expected);
    });
  });

  describe('uses a `normalizeRequestUrl` function that', () => {
    test(n(
      'does nothing and returns its argument',
      'if the path component argument does end with a hierarchy character'
    ), () => {
      const pathComponent = '/foo/bar';
      const sideEffect = jest.fn();

      window.location.assign(pathComponent);

      const actual = normalizeRequestUrl(sideEffect);
      const expected = pathComponent;

      expect(actual).toBe(expected);
      expect(sideEffect).not.toHaveBeenCalled();
    });

    test(n(
      'calls the side effect and returns the default route',
      'if the path component argument does not end with a hierarchy character'
    ), () => {
      const pathComponent = '/foo';
      const sideEffect = jest.fn();

      window.location.assign(pathComponent);

      const actual = normalizeRequestUrl(sideEffect);
      const expected = `${pathComponent}/${DEFAULT_SEGMENT}`;

      expect(actual).toBe(expected);
      expect(sideEffect).toHaveBeenCalledWith(expected);
    });
  });
});
