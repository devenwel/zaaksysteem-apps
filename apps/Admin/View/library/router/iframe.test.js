import { getIframeUrl, getParentUrl } from './iframe';

describe('The `iframeRouter` module', () => {
  /**
   * @test {getIframeUrl}
   */
  test('`getIframeUrl`', () => {
    expect(typeof getIframeUrl).toBe('function');
  });

  /**
   * @test {getParentUrl}
   */
  test('`getParentUrl`', () => {
    expect(typeof getParentUrl).toBe('function');
  });
});
