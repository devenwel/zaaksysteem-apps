/**
 * iFrame base segment dictionary.
 *
 * @type {Object}
 */
const dictionary = {
  catalogus: '/beheer/bibliotheek',
  gebruikers: '/medewerker',
  logboek: '/beheer/logging',
  transacties: '/beheer/sysin/transactions',
  koppelingen: '/beheer/sysin/overview',
  gegevens: '/beheer/object/datastore',
  configuratie: '/beheer/configuration',
  import: '/beheer/import',
  zaaktypen: '/beheer/zaaktypen',
};

export default dictionary;
