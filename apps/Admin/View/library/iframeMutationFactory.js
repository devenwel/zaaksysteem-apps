/**
 * @type {number}
 */
const ELEMENT_NODE = 1;

/**
 * @private
 * @param {HTMLElement} element
 * @return {boolean}
 */
const isModal = element => (
  element.hasAttribute('data-zs-modal')
  || element.hasAttribute('zs-modal')
);

/**
 * @private
 * @param {NodeList} nodeList
 * @return {boolean}
 */
const hasModal = nodeList =>
  [...nodeList]
    .filter(node => (node.nodeType === ELEMENT_NODE))
    .some(node => isModal(node));

/**
 * @private
 * @param {string} type
 * @param {string} name
 * @return {boolean}
 */
const isStyleAttribute = (type, name) => (
  (type === 'attributes')
  && (name === 'style')
);

/**
 * @private
 * @param {HTMLElement} element
 * @return {boolean}
 */
const isHiddenDialog = element => (
  element.classList.contains('ui-dialog')
  && element.style.display === 'none'
);

const isCustomDialogOverlay = target => (
  target.classList.contains('custom_overlay')
  && target.ownerDocument.querySelector('.ui-dialog')
);

/**
 * Create and expose a MutationObserver instance.
 * Tightly coupled to the iFramed legacy DOM.
 *
 * @param {HTMLElement} contextNode
 *   The iframe's DOM Node to observe.
 * @param {Function} onOverlayOpen
 *   Parent window callback.
 * @param {Function} onOverlayClose
 *   Parent window callback.
 * @return {MutationObserver}
 */
function iframeMutationFactory(contextNode, onOverlayOpen, onOverlayClose) {
  /**
   * Listen for the AngularJS Dialog directive.
   *
   * @private
   * @param {NodeList} addedNodes
   * @param {NodeList} removedNodes
   * @return {Function|undefined}
   */
  function handleChildList(addedNodes, removedNodes) {
    if (hasModal(addedNodes)) {
      return onOverlayOpen;
    }

    if (hasModal(removedNodes)) {
      return onOverlayClose;
    }
  }

  /**
   * Listen for the jQuery UI Dialog (very hairy).
   * Using `addedNodes` and `removedNodes` is possible and
   * faster, at the expense of a very noticable delay
   * between the iFrame and the parent window updates.
   *
   * @private
   * @param {HTMLElement} target
   * @return {Function|undefined}
   */
  function handleAttributes(target) {
    if (isCustomDialogOverlay(target)) {
      return onOverlayOpen;
    }

    if (isHiddenDialog(target)) {
      return onOverlayClose;
    }
  }

  /**
   * Cover multiple Dialog implementations.
   *
   * @private
   * @param {MutationRecord} mutation
   * @param {NodeList} mutation.addedNodes
   * @param {string} mutation.attributeName
   * @param {NodeList} mutation.removedNodes
   * @param {HTMLElement} mutation.target
   * @param {string} mutation.type
   * @return {Function|undefined}
   */
  function getCallback({
    addedNodes,
    attributeName,
    removedNodes,
    target,
    type,
  }) {
    if (type === 'childList') {
      return handleChildList(addedNodes, removedNodes);
    }

    if (isStyleAttribute(type, attributeName)) {
      return handleAttributes(target);
    }
  }

  /**
   * @param {MutationRecord} sequence
   */
  function iframeMutationCallback(sequence) {
    for (const record of sequence) {
      const callback = getCallback(record);

      if (callback) {
        callback();
        break;
      }
    }
  }

  /**
   * The MutationObserver instance is the only exposed value.
   * It's the consumer's own responsibility to disconnect.
   *
   * @type {MutationObserver}
   */
  const iframeObserver = new MutationObserver(iframeMutationCallback);

  iframeObserver
    .observe(contextNode, {
      attributes: true,
      childList: true,
      subtree: true,
    });

  return iframeObserver;
}

export default iframeMutationFactory;
