# Admin app `/View`

## Structure

- React component files and directories are upper camel cased
- components that are not tightly coupled to a 
  route base segment component go into `./Shared`
- route base segment components go into 
  one directory per base segment
- modules that are tightly coupled to the view but 
  do not export React components go into `./library`
