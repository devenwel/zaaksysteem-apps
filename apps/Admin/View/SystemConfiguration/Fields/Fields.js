import React from 'react';
import { withStyles, Form } from '@mintlab/ui';
import validate from '../../../../library/validation/validation';
import fieldsStyleSheet from './Fields.style';

function validateWithTranslation(validate, t) {
  return (...args) => {
    const error = validate(...args);
    if (error) {
      return t(`validation:${error}`);
    }
  };
}

export const ItemsForm = ({
  fieldSets,
  t,
  save, 
  set,
  identifier,
  classes,
  changed,
  headerRef,
}) => (
  <section className={ classes.section }>
    <Form
      fieldSets = { fieldSets }
      set = { set }
      save = { save }
      validate = { validateWithTranslation(validate, t) }
      identifier = { identifier }
      classes={ classes }
      changed = { changed }
      statusTarget = { headerRef }
    >
    </Form>
  </section>
);

export default withStyles(fieldsStyleSheet)(ItemsForm);
