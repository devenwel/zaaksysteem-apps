const GREY_VALUE = 200;

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const fieldsStyleSheet = ({
  palette: {
    grey,
  },
  breakpoints,
}) => ({
  section: {
    overflowX: 'auto',
    overflowY: 'scroll',
    padding: '30px',
    backgroundColor: grey[GREY_VALUE],
  },
  form: {
    [breakpoints.up('lg')]: {
      maxWidth: breakpoints.values.lg,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
});
