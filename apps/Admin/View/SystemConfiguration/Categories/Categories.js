import React from 'react';
import { withStyles } from '@mintlab/ui';
import { categoriesStyleSheet } from './Categories.style';
import { IconLink } from '@mintlab/ui';
import classNames from 'classnames';

export const Categories = ({
  categories,
  invoke,
  classes,
}) => {
  const routeTo = slug => {
    invoke({
      path: `/admin/configuratie/${slug}`,
    });
  };

  return (
    <ul className={ classes.categories }>
      {categories && categories.map(({
        slug,
        icon,
        current,
        name,
      }, i) =>
        <li key={ i }>

          <IconLink
            action={routeTo.bind(this, slug)}
            icon={icon}
            classes={{
              root: classes.regularButton,
              label: classes.label,
              active: classNames({[classes.activeButton]: current}),
              shadow: classes.shadow,
            }}
            active={current}
          >
            {name}
          </IconLink>

        </li>
      )}
    </ul>
  );
};

export default withStyles(categoriesStyleSheet)(Categories);
