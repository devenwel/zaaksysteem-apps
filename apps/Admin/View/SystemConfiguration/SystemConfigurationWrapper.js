import React from 'react';
import Resource from '../Shared/ResourceContainer';
import SystemConfigurationContainer from './SystemConfigurationContainer';

/**
 * Wrap the {@link SystemConfigurationContainer} with a {@link Resource}.
 *
 * @return {ReactElement}
 */
const SystemConfigurationWrapper = () => (
  <Resource
    id={'config'}
  >
    <SystemConfigurationContainer />
  </Resource>
);

export default SystemConfigurationWrapper;
