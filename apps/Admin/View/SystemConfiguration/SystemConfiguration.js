import React, { Component } from 'react';
import Categories from './Categories/Categories';
import Fields from './Fields/Fields';
import { systemConfigurationStyleSheet } from './SystemConfiguration.style.js';
import { withStyles, Title } from '@mintlab/ui';
import SnackbarContainer from '../Shared/Snackbar/SnackbarContainer';
import DialogContainer from '../Shared/Dialog/DialogContainer';
import { isPopulatedArray } from '../../../library/array';

/**
 * @param {Object} props
 * @param {Function} props.invoke
 * @param {Array} props.categories
 * @param {Function} props.set
 * @param {Function} props.save
 * @param {Array} props.fieldSets
 * @param {Function} props.t
 * @param {String} props.identifier
 * @param {Object} props.classes
 * @param {Array} props.changed
 * @return {ReactElement}
 */
export class SystemConfiguration extends Component {
  constructor(props) {
    super(props);
    this.headerRef = React.createRef();
  }

  render() {
    const {
      invoke,
      categories,
      set,
      save,
      fieldSets,
      t,
      identifier,
      classes,
      changed,
    } = this.props;

    return (
      <div className={ classes.wrapper }>

        <header className={ classes.header } ref={ this.headerRef }>
          <Title classes={{
            title: classes.title,
          }}>{ t('systemConfiguration:title') }</Title>
        </header>

        <section className={ classes.grid }>
          <Categories
            categories = { categories }
            invoke = { invoke }
          />

          <Fields
            fieldSets = { fieldSets }
            changed = { isPopulatedArray(changed) }
            set = { set }
            save = { save }
            t = { t }
            identifier = { identifier }
            headerRef={ this.headerRef.current }
          />
        </section>

        <DialogContainer />
        <SnackbarContainer />

      </div>
    );
  }
}

export default withStyles(systemConfigurationStyleSheet)(SystemConfiguration);
