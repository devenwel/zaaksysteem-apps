const GREY_VALUE = 50;

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const systemConfigurationStyleSheet = ({
  palette: {
    grey,
  },
}) => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
  grid: {
    display: 'grid',
    'grid-template-columns': '240px auto',
    height: 'calc(100% - 84px)',
  },
  header: {
    backgroundColor: grey[GREY_VALUE],
    padding: '12px 20px 12px 30px',
    height: '60px',
    display: 'flex',
    'align-items': 'center',
    'justify-content': 'flex-end',
  },
  title: {
    marginRight: 'auto',
  },
});

