import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { fieldsChanged } from '../../../library/form';
import { appToDb } from '../../Resource/API/transformers';
import SystemConfiguration from './SystemConfiguration';
import Actions from '../../Store/Action';
import update from 'immutability-helper';
import get from '../../../library/get';
import fieldSetsMap from '../../../data/systemConfiguration';

const { entries, assign } = Object;
const { parse, stringify } = JSON;
const DEFAULT_ROWS = 200;

const mapStateToProps = ({ route, form, resource, ui }) => ({ route, form, resource, ui });

const mapDispatchToProps = dispatch => ({
  invoke: bindActionCreators(Actions.route.invoke, dispatch),
  set: bindActionCreators(Actions.form.set, dispatch),
  request: bindActionCreators(Actions.resource.request, dispatch),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    resource: {
      config: {
        data: {
          items:resourceItems,
        },
      },
    },
    route,
    form,
  } = stateProps;

  const { request } = dispatchProps;
  const createCustomChoicesRequest = ({ url, name, input, match, rows }) =>
    request([
      {
        id: 'choices',
        method: 'GET',
        url,
        meta: {
          name,
        },
      },
      {
        [`query:match:${match}`]: input,
        'rows': rows || DEFAULT_ROWS,
      },
    ]);

  /**
   * Replace the item placeholders from the configuration file with the
   * actual definitions
   */
  /* eslint complexity: [2, 5] */
  const replaceFields = obj => {
    for (let [, value] of entries(obj)) {
      if (value.hasOwnProperty('fields')) {
        value.fields = value.fields.reduce((accumulator, name) => {
          const item = resourceItems.find(resourceItem => resourceItem.name === name);
          if (item) {
            const choicesParameters = get(item, 'choicesParameters');
            const mergedItem = assign({}, item, {
              value: form.hasOwnProperty(name) ? form[name] : get(item, 'value'),
              getChoices: choicesParameters ? input =>
                createCustomChoicesRequest(assign({}, choicesParameters, { input } ))
                : undefined,
            });

            accumulator.push(mergedItem);
          }
          return accumulator;
        }, []);
      } else if (typeof value === 'object' || Array.isArray(value)) {
        replaceFields(value);
      }
    }
  };
  let fullMap = parse(stringify(fieldSetsMap));

  replaceFields(fullMap);

  fullMap = fullMap.map(category =>
    update(category, {
      $merge: {
        current: route.split('/').slice('-1').toString() === category.slug,
      },
    })
  );

  const categories = fullMap.map(({ name, icon, slug, current }) => ({ name, icon, slug, current }));
  const [current] = fullMap.filter(({ current }) => current);
  const identifier = current ? current.slug : window.location.pathname;
  const fieldSets = get(current, 'fieldSets');
  const changed = fieldsChanged(form, resourceItems);

  /**
   * Dispatch a 'config:save' with changed items as the payload
   */
  const save = () => {
    const { request } = dispatchProps;
    const items = changed.reduce((accumulator, field) => {
      const { reference, value } = field;

      accumulator[reference] = {
        value: appToDb(value),
      };
      return accumulator;
    }, {});

    request(['config:save', { items }]);
  };

  return Object.assign({}, stateProps, dispatchProps, ownProps, {
    categories,
    fieldSets,
    save,
    identifier,
    changed,
  });
};

/**
 * Connects {@link SystemConfiguration} with {@link i18next} and the store.
 *
 * @return {ReactElement}
 */
const SystemConfigurationContainer = translate()(connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(SystemConfiguration));

export default SystemConfigurationContainer;
