import { createElement } from 'react';
import { render } from 'react-dom';
import AdminApp from './Shared/App/Provider';
import '@mintlab/ui/distribution/ui.css';
import './render.css';

const rootNode = document.getElementById('zs-app');

render(createElement(AdminApp), rootNode);

if (module.hot) {
  module.hot.accept();
}
