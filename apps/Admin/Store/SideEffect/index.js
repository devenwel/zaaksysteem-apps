import routeSideEffects from './route';
import resourceSideEffects from './resource';
import uiSideEffects from './ui';
import configSaveEffects from './configSave';
import choicesSideEffects from './choices';

const list = [
  resourceSideEffects,
  routeSideEffects,
  uiSideEffects,
  configSaveEffects,
  choicesSideEffects,
];

export default list.reduce((accumulator, sideEffects) => {
  Object
    .entries(sideEffects)
    .forEach(sideEffect => {
      const [key, func] = sideEffect;

      if (!accumulator[key]) {
        accumulator[key] = [];
      }

      accumulator[key].push(func);
    });

  return accumulator;
}, {});
