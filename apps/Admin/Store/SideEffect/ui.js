import action from '../Action';
import get from '../../../library/get';
import i18n from '../../../library/i18next';

const {
  resource: {
    respond,
  },
  ui: {
    snackbar: {
      show,
    },
  },
} = action;

/*eslint-disable */
export default {
  [respond](data) {

    const [payload] = Object.values(data);
    const status = get(payload, '$set.status');
    const resolve = action => (action) ? Promise.resolve(() => action) : Promise.resolve();

    if (!status) {
      return resolve();
    }

    // Error occurred
    if (status !== 200) {

      // Determine error type
      const { $set: {data }} = payload;
      let type = get(data, 'result.instance.type');

      // Not found, use status code
      if (!type || typeof(type === 'object')) {
        return resolve(show(`server:status${status}`));
      }

      // Remove everything before the last slash
      type = type.replace(/^(.*[\\\/])/gi, '');

      const key = (i18n.exists(`server:${type}`)) ? `server:${type}` : 'server:server_error';

      return resolve(show(key));
    }

    // Config saved
    if (data.hasOwnProperty('config:save')) {
      return resolve(show('config:saved'));
    }

    return resolve();
  }

};
