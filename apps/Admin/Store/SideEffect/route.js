import action from '../Action';
import { pushState } from '../../../library/dom/history';

const {
  route: {
    invoke,
    resolve,
  },
} = action;

export default {
  [invoke](parameters) {
    pushState(parameters.path);
    return Promise.resolve(() => resolve(parameters));
  },
};
