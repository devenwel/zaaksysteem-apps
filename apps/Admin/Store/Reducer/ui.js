import { createReducer } from '../../../library/redux/createReducer';
import action from '../Action';
import update from 'immutability-helper';

const { dialog, drawer, iframe, snackbar } = action.ui;

const initialState = {
  drawer: false,
  dialog: null,
  iframe: {
    overlay: false,
    loading: true,
  },
  snackbar: null,
};

/**
 * @type {Function}
 */
export const uiReducer = createReducer(initialState, {
  [dialog.show]:
    ({ payload, state }) => update(state, {
      dialog: {
        $set: payload,
      },
    }),
  [dialog.hide]:
    ({ state }) => update(state, {
      dialog: {
        $set: null,
      },
    }),
  [drawer.open]:
    ({ state }) => update(state, {
      drawer: {
        $set: true,
      },
    }),
  [drawer.close]:
    ({ state }) => update(state, {
      drawer: {
        $set: false,
      },
    }),
  [iframe.overlay.open]:
    ({ state }) => update(state, {
      iframe: {
        overlay: {
          $set: true,
        },
      },
    }),
  [iframe.overlay.close]:
    ({ state }) => update(state, {
      iframe: {
        overlay: {
          $set: false,
        },
      },
    }),
  [iframe.window.load]:
    ({ state }) => update(state, {
      iframe: {
        loading: {
          $set: false,
        },
      },
    }),
  [iframe.window.unload]:
    ({ state }) => update(state, {
      iframe: {
        loading: {
          $set: true,
        },
      },
    }),
  [snackbar.show]:
    ({ payload, state }) => update(state, {
      $merge: {
        snackbar: {
          timestamp: new Date().getTime(),
          message: payload,
        },
      },
    }),
  [snackbar.clear]:
    ({ state }) => update(state, {
      snackbar: {
        $set: null,
      },
    }),
});
