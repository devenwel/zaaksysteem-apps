import { createReducer } from '../../../library/redux/createReducer';
import action from '../Action';

const { resolve } = action.route;
const initialState = '/';

/**
 * @type {Function}
 */
export const routeReducer = createReducer(initialState, {
  [resolve]: ({ payload }) => payload.path,
});
