import { createReducer } from '../../../library/redux/createReducer';
import update from 'immutability-helper';
import action from '../Action';

/**
 * Payloads for these reducers must be in a valid
 * immutability-helper format
 * @see https://github.com/kolodny/immutability-helper
 */

const { assign } = Object;
const {
  resource: {
    request,
    respond,
  },
} = action;
const initialState = {};

const reduceRequest = ({
  payload,
  state,
}) => {

  const reduce = (accumulator, [value]) =>
    assign(accumulator, {
      [(value.hasOwnProperty('id')) ? value.id : value]: {$set: null},
    });

  return update(state, payload.reduce(reduce, {}));
};

const reduceResponse = ({
  payload,
  state,
}) => update(state, payload);

/**
 * @type {Function}
 */
export const resourceReducer = createReducer(initialState, {
  [request]: reduceRequest,
  [respond]: reduceResponse,
});
