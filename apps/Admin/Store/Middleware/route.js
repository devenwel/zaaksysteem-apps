const route = () =>
  next =>
    action =>
      next(action);

export default route;
