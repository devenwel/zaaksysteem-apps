import actions from '../Action/index';
import { fieldsChanged } from '../../../library/form';
import get from '../../../library/get';
import { getSegment } from '../../../library/url';

const {
  route: {
    resolve,
  },
  ui: {
    dialog: {
      show,
    },
  },
} = actions;

/**
 *
 * Intercepts the route 'resolve' action.
 * If any fields have been changed but not yet saved, cancel this action
 * and open a DiscardChanges dialog
 */
const route = store =>
  next =>
    action => {
      const { type, payload } = action;
      const state = store.getState();
      const { dispatch } = store;
      const form = get(state, 'form');
      const items = get(state, 'resource.config.data.items');

      const isConfigurationRoute = () => (
        (getSegment(payload.path) === 'configuratie')
        && (state.route !== payload.path)
        && (!payload.force)
      );

      const shouldDispatch = () => (
        type === String(resolve)
        && isConfigurationRoute()
        && fieldsChanged(form, items).length
      );

      if (shouldDispatch()) {
        dispatch(show({
          type: 'DiscardChanges',
          options: payload,
        }));

        return;
      }

      return next(action);
    };

export default route;
