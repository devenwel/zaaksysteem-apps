# Apps

> *Zaaksysteem Apps* are a collection of SPAs with a 
  flexible client-side technology stack that share a 
  unified build system in a dedicated Docker container.

## File system structure

An **App** is a directory that 
 
- MUST be a child of the `/apps` directory 
- MUST contain a `manifest.json` file

The directory name SHOULD be upper camel cased.

- /apps/
    - /ExampleApp
        - /Resource/
        - /Store/
        - /View/
        - /manifest.json

## manifest.json

The app manifest is a configuration object for the 
*build system* that MUST have

- **a `name` property**

  Used as the directory name in the server document root 
  for the build artefacts.

- **an `entry` property**

  The relative path to the application entry file.

- **a `profile` property** 

  The `profile` declares a 
  [**main development stack**](#major-development-stack).

### Example

    {
      "name": "admin",
      "entry": "View/entry.js",
      "profile": "react:redux"
    }

## Main development stack

A *main development stack* MUST consist of

- a **View** library
- a **State Manager** library

and has specialized presets in the *build system* configuration.

Stack identifiers MUST be their respective `npm` package names 
joined by a colon. 

### Examples
  
- `react:redux` (**implemented**)
- `react:mobx-state-tree`
- `react:react-automata`
- `vue:vuex`
- `angular:@ngrx/store`
