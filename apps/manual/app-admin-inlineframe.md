# —— Component: `InlineFrame`

> The `InlineFrame` component is the most complex component in the application.

## Purpose

apps/Admin/View/library/router/base.js

`/admin/foo/bar`

## Store

### State

- `ui`
    - `iframe`
        - `{boolean}` `loading`
        - `{boolean}` `overlay`

### Actions

- `ui`
    - `iframe`
        - `overlay`
            - `{Function}` `open`
            - `{Function}` `close`
        - `window`
            - `{Function}` `load`
            - `{Function}` `unload`

## `'react-redux'#connect`

The following properties are connected with `LayoutContainer` and passed 
through to `AdminLayout` to `View` to `IframeLoaderComponent`. 

### `hasIframeOverlay`

### `isIframeLoading`

State: 

- `ui.iframe.loading`

Action: 

- `ui.iframe.window.load`
- `ui.iframe.window.unload`

### `onIframeOpen`

### `onIframeClose`

### `onIframeLoad`

### `onIframeUnload`

### `InlineFrameLoader`
